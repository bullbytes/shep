# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.3] - 2017-02-04
### Added
- The user can save people in a database
- A table that shows the people from the database

## [0.0.2] - 2017-01-10
### Added
- Styles for person form
- User can add rows to person form with a button

## [0.0.1] - 2017-01-04
### Added
- Code for starting the server 
- README.md that explains how to start the server
- LICENSE containing the [BSD 2-Clause License](https://opensource.org/licenses/BSD-2-Clause)

