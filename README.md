# Shep
## Purpose
Keeps watch over other applications, the flock.

Shep contacts members of the flock and takes status reports from them.
Shep aggregates a summary from the individual reports.

## Get started
### Run the server locally
1. Install [SBT](http://www.scala-sbt.org/) 
2. Start SBT in your terminal, then fire up the server using `appJVM/run`
3. Point your browser to `localhost:8080`
4. Stop the running server using `<Ctrl>C` in your terminal

### Run the server locally in a Docker container
1. Install [SBT](http://www.scala-sbt.org/) and [Docker](https://docs.docker.com/engine/getstarted/step_one/)
2. To create an image from source, start SBT and issue `appJVM/docker`
4. To run the created image execute `docker-compose up` in your terminal
5. Point your browser to `localhost:8080`
6. Stop the running container using `docker-compose down`

### Platform specifics
* On Windows, make sure you share your Windows drive to make volumes work. Go to Docker's settings → Shared Drives

## Debugging
### Debugging the server
1. Create a [remote debugging configuration](https://www.jetbrains.com/help/idea/2016.2/run-debug-configuration-remote.html) that attaches to the JVM on port 5005
2. Start SBT like this: `sbt -D"dev.mode=true"`
3. Start debugging using the remote configuration

## Miscellaneous hints
* Don't forget to `reload` in the SBT console after making changes to `build.sbt`
* Use `~appJVM/docker` to create a new Docker image automatically whenever you edit the source code
* To create JavaScript faster, start SBT like this: `sbt -D"dev.mode=true"`. The generated JavaScript file will be larger, though


