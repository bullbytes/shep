package client

import client.pages.HelloPage
import client.pages.personform.PersonForm
import org.scalajs.dom._
import shared.pages.{PageId, PageIds}
import slogging.{LazyLogging, LoggerConfig, PrintLoggerFactory}

import scala.scalajs.js.JSApp

/**
  * This code is transpiled to JavaScript and runs in the browser.
  * On the client, the user can create new promotional articles, see an overview of their articles and edit articles.
  */
object Client extends JSApp with LazyLogging {
  // Log using Scala's println()
  LoggerConfig.factory = PrintLoggerFactory()

  def main(): Unit = executeClientCodeOnPage()

  /**
    * Executes the client code for the page. The client code is our Scala code transpiled to JavaScript.
    */
  private def executeClientCodeOnPage() =
    PageId(document.head.id) match {
      case PageIds.hello => HelloPage.execute()
      case PageIds.form => PersonForm.execute()
      case otherPageId => logger.info(s"There's no client code for page '$otherPageId'")
    }
}
