package client.ajax

import autowire._
import org.scalajs.dom
import shared.api.ShepAjaxApi
import shared.data.Person
import shared.serverresponses.ServerResponse

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Using Autowire, we can perform AJAX calls to the server in a type-safe manner.
  * <p>
  * Created by Matthias Braun on 1/9/2017.
  */
object Ajaxer extends autowire.Client[String, upickle.default.Reader, upickle.default.Writer] {
  def postPersons(persons: Seq[Person]): Future[ServerResponse] = Ajaxer[ShepAjaxApi].postPersons(persons).call()

  override def doCall(req: Request): Future[String] = {
    dom.ext.Ajax.post(
      url = "/" + ShepAjaxApi.AjaxPath + "/" + req.path.mkString("/"),
      data = upickle.default.write(req.args)
    ).map(_.responseText)
  }

  // There must be a Reader[Result] implicitly available
  def read[Result: upickle.default.Reader](string: String): Result = upickle.default.read[Result](string)

  // There must be a Writer[Result] implicitly available
  def write[Result: upickle.default.Writer](result: Result): String = upickle.default.write(result)
}


