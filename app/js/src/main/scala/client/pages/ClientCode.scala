package client.pages

import org.scalajs.dom._

import scalatags.JsDom.all._

/**
  * Objects of this trait contain the code that is run in the client's browser.
  * <p>
  * Created by Matthias Braun on 1/5/2017.
  */
trait ClientCode {

  /**
    * Appends a `nodeToAppend` to the page's document body.
    *
    * @param nodeToAppend we append this [[Node]] to the body of the document
    * @return the appended node
    */
  def bodyAppend(nodeToAppend: Node): Node = document.body.appendChild(nodeToAppend)

  /**
    * Appends a `tagToAppend` to the page's document body.
    *
    * @param tagToAppend we convert this [[HtmlTag]] to a [[Node]] and append it to the body of the document
    * @return the appended node
    */
  def bodyAppend(tagToAppend: HtmlTag): Node = bodyAppend(tagToAppend.render)

}
