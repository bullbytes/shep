package client.pages

import org.scalajs.dom._
import shared.api.ShepPaths

import scalatags.JsDom.all._

/**
  * This code will run as JavaScript on the `HelloPage`.
  * <p>
  * Created by Matthias Braun on 1/5/2017.
  */
object HelloPage extends ClientCode {

  private def onPageLoaded() = (_: Event) => bodyAppend(
    div(
      p("The ", b("client"), " added this with JavaScript. Or was it Scala? 😊"),
      a(href := ShepPaths.formPage)("Show me something more involved.")
    )
  )

  /**
    * Execute the client code on the page.
    */
  def execute(): Any = window.onload = onPageLoaded()
}
