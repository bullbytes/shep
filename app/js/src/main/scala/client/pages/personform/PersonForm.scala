package client.pages.personform

import java.util.UUID

import client.ajax.Ajaxer
import client.pages.ClientCode
import client.utils.HtmlIdImplicits.idToAttribute
import client.utils.{Elements, ErrorMsg}
import org.scalajs.dom._
import org.scalajs.dom.html.{Button, Div, Table, TableSection}
import shared.api.ShepPaths
import shared.data.Person
import shared.pages.elements.{PeopleFormElementIds => Ids}
import shared.pages.styles.{PeopleFormStyles => Styles}
import shared.serverresponses.{GotPersonsAndSavedInDb, ServerResponse}
import shared.utils.Eithers
import slogging.LazyLogging

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.Date
import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

/**
  * The client code for the person form.
  * <p>
  * Created by Matthias Braun on 1/6/2017.
  */
object PersonForm extends ClientCode with LazyLogging {

  private def createPersonId = UUID.randomUUID()

  private def personFromRowNr(rowNr: Int): Either[ErrorMsg, Person] = {
    val get = Elements.getValueOfInput _

    for {
      name <- get(Ids.personName(rowNr))
      age <- get(Ids.personAge(rowNr))
      occupation <- get(Ids.occupation(rowNr))
    } yield Person(createPersonId, name, age, occupation)
  }

  private def getPersonsFromTable: Either[ErrorMsg, Seq[Person]] =
    Elements.get[Table](Ids.peopleTable).flatMap(table => {
      // We subtract one from the number of rows since we don't parse the table's header row, only the rows of its body
      val tableBodyRows = table.rows.length - 1

      val errorMsgsAndPersons = (1 to tableBodyRows).map(personFromRowNr)

      // Return either the first error message or all the persons from the table
      val errorOrPersons = Eithers.sequence(errorMsgsAndPersons)

      // Filter out empty persons (i.e, persons from those rows where the user hasn't put any text)
      errorOrPersons.map(_.filter(_.nonEmpty))
    })

  private def addRow(): (Event => Any) = (_: Event) => {
    logger.info("Add row button was clicked")

    Elements.get[TableSection](Ids.peopleTableBody).fold(
      errorMsg => logger.warn(s"Didn't find table body so we can't add a row to it: $errorMsg"),
      tableBody => {
        // Our table rows start at #1
        val nextTableRowNr = 1 + tableBody.rows.length
        logger.info(s"Adding row #$nextTableRowNr")
        // Append a row
        tableBody.appendChild(PersonFormRows.create(nextTableRowNr))
      }
    )
  }

  def informUserAboutServerResponse(serverResponse: ServerResponse): Unit = {

    Elements.get[Div](Ids.serverResponseInfo) match {
      case Right(infoBox) =>
        val infoText = serverResponse match {
          case GotPersonsAndSavedInDb(msg) => s"Server responded: $msg"
          case other => s"Unexpected response from server: $other"
        }
        val currentTime = new Date().toLocaleTimeString()
        Elements.setChild(infoBox, span(s"$currentTime: $infoText").render)
        Elements.show(infoBox)

      case Left(error) => logger.warn(s"Couldn't find info box to show server response: $error")
    }
  }

  private def submitForm(): (Event => Any) = (_: Event) => {
    logger.info("Submit button was clicked")

    getPersonsFromTable.fold(
      errorMsg => logger.warn(s"Could not get persons from page: $errorMsg"),
      persons => {
        Ajaxer.postPersons(persons).map { serverResponse => informUserAboutServerResponse(serverResponse) }
      }
    )
  }

  def execute(): Unit =
  /* We execute our JavaScript after the page was loaded since we access the HTML elements of the page (which are
  unavailable as long as the page isn't loaded).*/
    window.onload = (_: Event) => onPageLoaded()

  private def addButtonListeners() = {
    Elements.get[Button](Ids.submitButton).fold(errorMsg =>
      logger.warn(s"Couldn't find submit button on person form: $errorMsg"),
      button => button.onclick = submitForm()
    )

    Elements.get[Button](Ids.addRowButton).fold(errorMsg =>
      logger.warn(s"Couldn't find add row button on person form: $errorMsg"),
      button => button.onclick = addRow()
    )
  }

  private def addRowsToTable(nrOfPersonsInTable: Int): Any =
    Elements.get[Table](Ids.peopleTable).fold(
      errorMsg => logger.warn(s"Didn't find table so we can't add a row to it: $errorMsg"),
      table => {
        logger.info("Adding initial rows")
        val tableBody = tbody(Ids.peopleTableBody)((1 to nrOfPersonsInTable).map(PersonFormRows.create))
        table.appendChild(tableBody.render)
      }
    )

  private def addInfoBoxForServerResponses() = {

    // Contains information for the user from the server. Initially empty and invisible
    val infoBox = div(Styles.infoBox)(Ids.serverResponseInfo).render
    Elements.hide(infoBox)
    bodyAppend(infoBox)
  }

  private def addLinkToPersonOverview(): Any = Elements.get[Div](Ids.tableWithButtons)
    .fold(
      errorMsg => logger.warn(s"Could not add link to person overview to page: $errorMsg"),
      _.appendChild(a(Styles.peopleOverviewLink)(href := ShepPaths.peopleOverviewPage)("The people you saved").render))

  private def onPageLoaded() = {
    logger.info("Let's execute some client code on the person form")

    // Let's show this many person rows in the table initially
    val nrOfPersonsInTable = 3
    addRowsToTable(nrOfPersonsInTable)

    addLinkToPersonOverview()

    // Informs the user how the server responded to requests such as sending it the person data from the table
    addInfoBoxForServerResponses()

    addButtonListeners()
  }
}
