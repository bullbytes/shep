package client.utils

import org.scalajs.dom.raw.{HTMLCollection, HTMLElement, HTMLInputElement}
import org.scalajs.dom.{Element, Node, document}
import org.scalajs.jquery.jQuery
import shared.pages.elements.HtmlId
import shared.pages.styles.PeopleFormStyles
import slogging.LazyLogging

import scala.reflect.ClassTag
import scala.scalajs.js
import scalacss.internal.StyleA

/**
  * Helps with accessing and creating DOM elements.
  * <p>
  * Created by Matthias Braun on 1/6/2017.
  */
object Elements extends LazyLogging {

  def getValueOfInput(inputId: HtmlId): Either[ErrorMsg, String] = get[HTMLInputElement](inputId).map(_.value)

  def get[T: ClassTag](elementId: HtmlId): Either[ErrorMsg, T] = {
    val queryResult = document.querySelector(s"#$elementId")
    queryResult match {
      case elem: T => Right(elem)
      case other => Left(ErrorMsg(s"Element with ID $elementId is $other"))
    }
  }

  def setChild(parent: HTMLElement, newChild: HTMLElement): Node = {
    removeAllChildren(parent)
    parent.appendChild(newChild)
  }

  def removeAllChildren(parent: HTMLElement): Unit =
    forEachElement(parent.children) { child => parent.removeChild(child) }

  def show(idOfElementToShow: HtmlId): Unit = removeStyle(idOfElementToShow, PeopleFormStyles.hidden)

  def show(elementToShow: HTMLElement): Unit = removeStyle(elementToShow, PeopleFormStyles.hidden)

  def hide(elementToHide: HTMLElement): Unit = addStyle(elementToHide, PeopleFormStyles.hidden)

  def hide(idOfElementToHide: HtmlId): Unit = addStyle(idOfElementToHide, PeopleFormStyles.hidden)

  def forEachElement(htmlCollection: HTMLCollection)(action: (Element => Unit)): Unit =
    (0 until htmlCollection.length)
      .map(htmlCollection.item)
      .foreach(action)

  def removeStyle(elementId: HtmlId, styleToRemove: StyleA): Unit =
    get[Element](elementId).fold(
      errorMsg => logger.warn(s"Can't remove class from element with ID $elementId: $errorMsg"),
      element => removeStyle(element, styleToRemove)
    )

  def removeStyle(element: js.Any, classToRemove: StyleA): Unit = jQuery(element).removeClass(classToRemove.htmlClass)

  def addStyle(elementId: HtmlId, styleToAdd: StyleA): Unit =
    get[Element](elementId).fold(
      errorMsg => logger.warn(s"Can't add class to element with ID $elementId: $errorMsg"),
      element => addStyle(element, styleToAdd)
    )

  def addStyle(element: js.Any, classToAdd: StyleA): Unit = jQuery(element).addClass(classToAdd.htmlClass)
}
