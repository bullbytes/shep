package client.utils

import org.scalajs.dom.Element
import shared.pages.elements.HtmlId

import scalatags.JsDom.all._
import scalatags.generic.AttrPair

/**
  * Provides implicit conversions for [[HtmlId]]s on the client.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object HtmlIdImplicits {
  /** Converts an [[HtmlId]] to an ID attribute used in ScalaTags on the client */
  implicit def idToAttribute(htmlId: HtmlId): AttrPair[Element, String] = id := htmlId.value
}
