
import server.ShepServer
import shepbuildinfo.BuildInfo
import slogging._

/**
  * Starts this application.
  */
object Main extends LazyLogging {

  /**
    * Prints runtime information such as the memory available to the JVM.
    */
  private def printRuntimeInfo() = {
    val runtime = Runtime.getRuntime
    val mb = 1024 * 1024
    val maxMemoryInMb = runtime.maxMemory() / mb
    logger.info(s"JVM max memory: $maxMemoryInMb MB")
  }

  def main(args: Array[String]): Unit = {

    // Without this, we wouldn't have log messages
    LoggerConfig.factory = SLF4JLoggerFactory()

    logger.info(s"Let's start the ${BuildInfo.name} server :-)")
    printRuntimeInfo()

    ShepServer.up()
  }
}
