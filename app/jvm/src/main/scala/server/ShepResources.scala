package server

/**
  * Provides paths to the resources of this applications, such as images.
  * <p>
  * Created by Matthias Braun on 1/13/2017.
  */
object ShepResources {
  val favicon = "images/favicon.ico"
}
