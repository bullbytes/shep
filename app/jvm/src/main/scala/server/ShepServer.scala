package server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import server.persistency.ShepDb
import server.routes.ShepRoutes
import shared.api.ShepAjaxApi
import shared.data.Person
import shared.serverresponses.{GotPersonsAndSavedInDb, GotPersonsButErrorWhileSaving, ServerResponse, ServerStringsForClient}
import slogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Properties, Success}

/**
  * The server lets clients add, change, and retrieve promotional articles.
  */
object ShepServer extends ShepAjaxApi with LazyLogging {

  /**
    * Starts the server.
    */
  def up(): Unit = {

    // Akka needs these vals to run our server
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    val port = Properties.envOrElse("SHEP_PORT", "8080").toInt
    val host = "0.0.0.0"

    Http().bindAndHandle(ShepRoutes.routes, host, port)

    logger.info(s"Server started. Host: $host:$port")
  }

  override def postPersons(persons: Seq[Person]): Future[ServerResponse] = {
    val size = persons.size
    val personOrPersons = if (size == 1) "person" else "persons"
    logger.info(s"Server got $size $personOrPersons from client")
    ShepDb.save(persons).map {
      case Success(result) => GotPersonsAndSavedInDb(ServerStringsForClient.gotPersons(persons))
      case Failure(error) => GotPersonsButErrorWhileSaving(ServerStringsForClient.errorWhileSavingPersons(persons, error))
    }
  }
}
