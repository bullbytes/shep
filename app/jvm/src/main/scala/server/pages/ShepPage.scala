package server.pages

import shared.pages.PageId

import scala.concurrent.Future

/**
  * All the web pages in this application share this trait.
  * <p>
  * Created by Matthias Braun on 1/5/2017.
  */
trait ShepPage {
  def pageId: PageId

  // Creating the HTML might involve a lengthy process such as querying the database. Thus, the pages don't need
  // to provide their HTML right now but in the future
  def mkHtml: Future[String]
}
