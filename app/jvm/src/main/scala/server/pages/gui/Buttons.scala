package server.pages.gui



import scalacss.ScalatagsCss._
import scalacss.internal.StyleA
import scalatags.Text.TypedTag
import scalatags.Text.all._

/**
  * Provides HTML buttons.
  * <p>
  * Created by Matthias Braun on 1/6/2017.
  */
object Buttons {

  def withTextAndIcon(text: String, icon: TypedTag[String], styles: Seq[StyleA]): TypedTag[String] = {
    val textAndIcon = div(icon, span(s" $text"))
    button(styles)(`type` := "button")(textAndIcon)
  }

  def withTextAndIcon(text: String, icon: TypedTag[String], style: StyleA): TypedTag[String] =
    withTextAndIcon(text, icon, Seq(style))

  def withText(text: String, style: StyleA): TypedTag[String] = withTextAndIcon(text, div(), style)

}
