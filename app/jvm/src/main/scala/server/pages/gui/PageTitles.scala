package server.pages.gui

/**
  * Provides the titles of web pages.
  * <p>
  * Created by Matthias Braun on 1/4/2017.
  */
object PageTitles {

  val hello = PageTitle("Hello there")
  val form = PageTitle("Person form")
  val peopleOverview = PageTitle("People overview")

  val notFound = PageTitle("Page not found")
}

case class PageTitle(value: String) extends AnyVal {
  override def toString: String = value
}
