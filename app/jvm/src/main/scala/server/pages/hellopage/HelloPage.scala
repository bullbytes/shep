package server.pages.hellopage

import server.pages.ShepPage
import server.pages.gui.PageTitles
import server.pages.utils.PageUtils
import shared.pages.{PageId, PageIds}

import scala.concurrent.Future
import scalatags.Text.all._

/**
  * A simple hello world HTML page which the server sends to the client.
  * <p>
  * Created by Matthias Braun on 10/10/2016.
  */
object HelloPage extends ShepPage {

  override def pageId: PageId = PageIds.hello

  /**
    * Builds this page as a string of HTML.
    *
    * @return the page as HTML
    */
  override def mkHtml: Future[String] = Future.successful(html(mkHeader, mkBody).render)

  private def mkBody = PageUtils.bodyWithApp(
    div(
      h1("A minimal Scala.js example"),
      p("The ", b("server"), " added this. Time to relax ☕")
    )
  )

  private def mkHeader = PageUtils.defaultHeader(pageId, PageTitles.hello)
}
