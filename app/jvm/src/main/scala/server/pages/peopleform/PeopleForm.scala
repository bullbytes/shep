package server.pages.peopleform

import server.pages.ShepPage
import server.pages.gui.{PageTitles, PeopleTableHelper}
import server.pages.utils.HtmlIdImplicits.idToAttribute
import server.pages.utils.PageUtils
import shared.pages.elements.{PeopleFormElementIds => Ids}
import shared.pages.styles.{PeopleFormStyles => Styles}
import shared.pages.{PageId, PageIds}
import slogging.LazyLogging

import scala.concurrent.Future
import scalacss.ScalatagsCss._
import scalatags.Text.TypedTag
import scalatags.Text.all._

/**
  * A simple form where users can fill in data about people and submit it the server.
  * <p>
  * Created by Matthias Braun on 1/5/2017.
  */
object PeopleForm extends ShepPage with LazyLogging {

  override def pageId: PageId = PageIds.form

  private def peopleTable =
    div(
      // The server only adds the table header. The client provides the table body containing the rows
      table(Styles.defaultTable)(Ids.peopleTable)(PeopleTableHelper.head),
      // The user can add more rows to the table by pressing this button. The client will add the row
      button(Styles.addRowButton)(Ids.addRowButton, `type` := "button")("Add row")
    )

  private def addSubmitButton(table: TypedTag[String]) = {
    val submitButton = button(Styles.submitButton)(Ids.submitButton, `type` := "button")("Save people")
    // Styling the table as a form applies the Pure styles to the table's input fields
    form(Styles.form)(div(table, submitButton))
  }

  override def mkHtml: Future[String] = Future.successful(html(pageHeader, pageBody).render)

  private def pageHeader = PageUtils.defaultHeader(pageId, PageTitles.form)

  private def pageBody = PageUtils.bodyWithAppAndStyle(Styles) {
    div(Ids.tableWithButtons)(Styles.centered)(h1("An example form"), addSubmitButton(peopleTable))
  }
}
