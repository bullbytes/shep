package server.pages.peopleoverview

import server.pages.ShepPage
import server.pages.gui.PageTitles
import server.pages.utils.PageUtils
import shared.pages.styles.PeopleOverviewStyles
import shared.pages.{PageId, PageIds}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalacss.ScalatagsCss._
import scalatags.Text.all._

/**
  * Shows all the people that we saved in our database.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object PeopleOverview extends ShepPage {
  override def pageId: PageId = PageIds.peopleOverview

  override def mkHtml: Future[String] = getPageBody.map(pageBody => html(pageHeader, pageBody).render)

  private def pageHeader = PageUtils.defaultHeader(pageId, PageTitles.peopleOverview)

  private def getPageBody = PeopleTable.create.map(table =>
    PageUtils.bodyWithAppAndStyle(PeopleOverviewStyles) {
      div(PeopleOverviewStyles.centered)(h1("All the people"), table)
    }
  )
}
