package server.pages.peopleoverview

import server.pages.gui.PeopleTableHelper
import server.persistency.ShepDb
import shared.data.Person
import shared.pages.styles.{PeopleOverviewStyles, SharedStyles}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success
import scalacss.ScalatagsCss._
import scalatags.Text.TypedTag
import scalatags.Text.all._

/**
  * Shows all the people in the database.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object PeopleTable {

  private def toRow(person: Person): TypedTag[String] = tr(td(person.name), td(person.age), td(person.occupation))

  private def getPeopleAsRows: Future[TypedTag[String]] = ShepDb.getAllPeople.map {
    case Success(people) => div(people.map(toRow))
    case error => p(s"Unable to get people from database. Error: $error")
  }

  def create: Future[TypedTag[String]] =
    getPeopleAsRows.map(peopleAsRows =>
      table(PeopleOverviewStyles.table)(PeopleTableHelper.head, tbody(peopleAsRows))
    )
}
