package server.pages.utils


import shared.pages.elements.HtmlId

import scalatags.Text.all._
import scalatags.generic.AttrPair
import scalatags.text.Builder

/**
  * Provides implicit conversions for [[HtmlId]]s on the server.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object HtmlIdImplicits {

  /** Converts an [[HtmlId]]s to an ID attribute used in ScalaTags on the server */
  implicit def idToAttribute(htmlId: HtmlId): AttrPair[Builder, String] = id := htmlId.value
}
