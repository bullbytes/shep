package server.pages.utils

import java.nio.charset.StandardCharsets
import java.util.Locale

import server.pages.gui.PageTitle
import shared.pages.PageId
import shepbuildinfo.BuildInfo

import scalacss.Defaults._
import scalacss.ScalatagsCss._
import scalacss.internal.mutable.StyleSheet.Inline
import scalatags.Text.TypedTag
import scalatags.Text.all._
import scalatags.Text.tags2.title

/**
  * Helps building web pages which the servers sends to the client.
  * <p>
  * Created by Matthias Braun on 1/4/2017.
  */
object PageUtils {

  // The application name as defined in build.sbt in lowercase
  private val appNameLower = BuildInfo.name.toLowerCase(Locale.ROOT)
  // Calls the main method of our jsApp. https://www.scala-js.org/tutorial/basic/#automatically-creating-a-launcher
  val launcher: String = "/" + appNameLower + "-launcher.js"
  // The client code we've written in Scala, translated to JavaScript
  val jsApp: String = "/" + appNameLower + ".js"

  // Client-side dependencies such as, for example, jQuery
  val jsDependencies = s"/$appNameLower-jsdeps.min.js"

  // Used for styling our web page
  val pureCss = "https://cdn.jsdelivr.net/pure/0.6.1/pure-min.css"

  // Defines which character encoding our web page uses
  val utf8: String = StandardCharsets.UTF_8.displayName()

  /**
    * Creates the default HTML header for pages of this application. It includes encoding and the client code for this
    * application.
    *
    * @param pageId    used to identify this page and execute the correct JavaScript/Scala code at the client.
    * @param pageTitle the title of this page, appears in the browser's tab
    * @return the default header for a page of this application
    */
  def defaultHeader(pageId: PageId, pageTitle: PageTitle): TypedTag[String] = {

    head(id := pageId.value)(
      meta(charset := utf8),
      title(pageTitle.value),
      script(src := jsApp),
      // Include the dependencies such as jQuery
      script(src := jsDependencies),

      link(
        rel := "stylesheet",
        href := pureCss
      )
    )
  }

  /**
    * Creates an HTML body containing the client code that is executed as JavaScript on the client. Also adds the
    * given `styleSheet` to the body.
    *
    * @param styleSheet the CSS style sheet to add to the page's body
    * @param content    the content of the body in addition to the client code
    * @return the `content` and the client code that will run as JavaScript on the page
    */
  def bodyWithAppAndStyle(styleSheet: Inline)(content: TypedTag[String]): TypedTag[String] =
  // We add the inline style sheet to the body instead of the header since that makes browsers remover the header's IP
    body(styleSheet.render)(
      // Launches the client code
      script(src := PageUtils.launcher),
      content
    )

  /**
    * Creates an HTML body containing the client code that is executed as JavaScript on the client.
    *
    * @param content the content of the body in addition to the client code
    * @return the `content` and the client code that will run as JavaScript on the page
    */
  def bodyWithApp(content: TypedTag[String]): TypedTag[String] =
    bodyWithAppAndStyle(NoStyle)(content)

  object NoStyle extends StyleSheet.Inline

}
