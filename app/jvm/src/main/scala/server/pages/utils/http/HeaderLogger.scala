package server.pages.utils.http

import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.server.{Directives, Route}
import server.pages.ShepPage
import server.routes.ShepRoutes
import slogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Logs [[HttpHeader]]s of clients that use our [[ShepRoutes]].
  * <p>
  * Created by Matthias Braun on 1/9/2017.
  */
object HeaderLogger extends Directives with LazyLogging {

  /**
    * Logs the headers of the client who performed an AJAX request and returns the `innerRoute`
    *
    * @param methodToCall the method to call for the AJAX request as a list of package names, object or class,
    *                     and finally the method name
    * @param innerRoute   we return this [[Route]] after having logged the client's header
    * @return the `innerRoute`
    */
  def log(methodToCall: Seq[String])(innerRoute: Route): Route = {
    val formattedMethodToCall = methodToCall.mkString(".")
    val logHeaders = (headers: Seq[HttpHeader]) =>
      logger.info(s"Ajax request '$formattedMethodToCall' from client with these headers: ${format(headers)}")

    processHeaders(logHeaders, innerRoute)
  }

  private def processHeaders(headerFunc: (Seq[HttpHeader] => Unit), innerRoute: Route): Route =
  /* To get the IP address of the requesting machine, we need to have this piece of configuration in resources/application.conf:
      server {
      remote-address-header = on
      ...
   */
    extract(_.request.headers) { headers =>
      headerFunc(headers)
      innerRoute
    }

  def log(requestedPage: ShepPage, innerRoute: Route): Route = {
    val logHeaders = (headers: Seq[HttpHeader]) =>
      logger.info(s"Request for page '${requestedPage.pageId}' from client with these headers: ${format(headers)}")

    processHeaders(logHeaders, innerRoute)
  }

  private def format(headers: Seq[HttpHeader]) = headers.map(header => s"${header.name}: ${header.value}").mkString(", ")

}
