package server.pages.utils.http

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCode}
import akka.http.scaladsl.server.{Directives, Route}
import server.pages.ShepPage

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Helps with creating HTTP responses.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object HttpHelper extends Directives {

  def logHeadersAndRespondWith(page: ShepPage): Route = HeaderLogger.log(page, complete(HttpHelper.html(page)))

  /** Responds with a `page` and include the given `statusCode` in the response. */
  def respond(page: ShepPage, statusCode: StatusCode): Route =
    complete(
      page.mkHtml.map(pageContent => HttpResponse(statusCode, entity = htmlUtf8Entity(pageContent)))
    )

  private def htmlUtf8Entity(content: String) = HttpEntity(ContentTypes.`text/html(UTF-8)`, content)

  /** Creates an HTML response for the client with the content of the given `page`. */
  private def html(page: ShepPage) = page.mkHtml.map(htmlUtf8Entity)


}
