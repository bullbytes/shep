package server.persistency

import java.util.UUID

import shared.data.Person
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.meta.MTable
import slogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Try

/**
  * Lets us save and retrieve data from a database.
  * <p>
  * Created by Matthias Braun on 1/12/2017.
  */
object ShepDb extends LazyLogging {

  // This configuration is in src/main/resources/application.conf
  private val postgresConfig = "pg-postgres"
  private val db = Database.forConfig(postgresConfig)

  private val persons = TableQuery[PersonTable]

  private def setUpDb(): Unit = {
    logger.info("Setting up database")
    // Get all existing tables
    val tables = Await.result(db.run(MTable.getTables), 10 seconds)

    val personTableName = persons.baseTableRow.tableName
    if (!tables.exists(existingTable => existingTable.name.name == personTableName)) {
      logger.info(s"Creating table '$personTableName'")
      Await.result(db.run(persons.schema.create), 10 seconds)
    } else {
      logger.info(s"Table '$personTableName' already exists")
    }
    logger.info("Finished setting up shep database")
  }

  setUpDb()


  class PersonTable(tag: Tag) extends Table[Person](tag, "persons") {
    def id = column[UUID]("id", O.PrimaryKey)

    def name = column[String]("name")

    def age = column[String]("age")

    def occupation = column[String]("occupation")

    def * = (id, name, age, occupation) <> (Person.tupled, Person.unapply)
  }

  def getAllPeople: Future[Try[Seq[Person]]] = db.run(persons.result.asTry)

  def save(personsToAdd: Seq[Person]): Future[Try[Option[Int]]] = {
    val saveAction = persons ++= personsToAdd
    db.run(saveAction.asTry)
  }
}
