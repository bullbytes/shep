package server.routes

import akka.http.scaladsl.server.{Directives, Route}
import server.ShepServer
import server.pages.utils.http.HeaderLogger
import shared.api.ShepAjaxApi
import upickle.default.{Reader, Writer}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
/**
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object Ajaxer extends Directives {

  def respond: Route =
  post {
    path(ShepAjaxApi.AjaxPath / Segments) { methodToCall =>
      decodeRequest {
        entity(as[String]) { requestData =>
          HeaderLogger.log(methodToCall) {
            complete(handleAjaxRequest(methodToCall, requestData))
          }
        }
      }
    }
  }

  /**
    * Used for AJAX calls from the client.
    */
  private object AjaxRouter extends autowire.Server[String, Reader, Writer] {

    // There must be a Reader[Result] implicitly available
    def read[Result: Reader](string: String): Result = upickle.default.read[Result](string)

    // There must be a Writer[Result] implicitly available
    def write[Result: Writer](result: Result): String = upickle.default.write(result)
  }

  /**
    * Handles an AJAX request sent from the client.
    *
    * @param methodToCall
    *                    contains the package, the class, and the name of the API method to call
    * @param requestData the request data from the client, such as JSON
    * @return the future answer to the request
    */
  private def handleAjaxRequest(methodToCall: List[String], requestData: String): Future[String] = {

    // Turn the JSON into a map
    val requestDataAsMap = upickle.default.read[Map[String, String]](requestData)
    val request = autowire.Core.Request(methodToCall, requestDataAsMap)
    // The future answer to the request
    AjaxRouter.route[ShepAjaxApi](ShepServer)(request)
  }
}
