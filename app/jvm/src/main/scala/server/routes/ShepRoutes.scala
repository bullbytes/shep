package server.routes

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.{Directives, RejectionHandler, Route}
import akka.stream.Materializer
import server.ShepResources
import server.pages.NotFoundPage
import server.pages.hellopage.HelloPage
import server.pages.peopleform.PeopleForm
import server.pages.peopleoverview.PeopleOverview
import server.pages.utils.http.HttpHelper
import shared.api.ShepPaths
import slogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Defines what happens when a client sends requests to the server.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object ShepRoutes extends Directives with LazyLogging {

  /** Handles the case when the client tries to visit a page of this application that doesn't exist */
  private def shepRejectionHandler =
    RejectionHandler.newBuilder()
      .handleNotFound(HttpHelper.respond(NotFoundPage, StatusCodes.NotFound))
      .result()

  /**
    * Maps paths in the URL to actions.
    *
    * @param materializer needed to run the server and its routes
    * @return a route for this application
    */
  def routes(implicit materializer: Materializer): Route =
    handleRejections(shepRejectionHandler) {
      // Include the JavaScript files in the response
      getFromResourceDirectory("") ~
        get {
          pathSingleSlash(
            redirect(ShepPaths.helloPage, StatusCodes.TemporaryRedirect)
          )
        } ~
        (path(ShepPaths.helloPage) & get) {
          HttpHelper.logHeadersAndRespondWith(HelloPage)
        } ~
        (path(ShepPaths.formPage) & get) {
          HttpHelper.logHeadersAndRespondWith(PeopleForm)
        } ~
        (path(ShepPaths.peopleOverviewPage) & get) {
          HttpHelper.logHeadersAndRespondWith(PeopleOverview)
        } ~
        path("favicon.ico") {
          getFromResource(ShepResources.favicon, MediaTypes.`image/x-icon`)
        } ~
        // Respond to an AJAX request
        Ajaxer.respond
    }


}
