package shared.api

import shared.data.Person
import shared.serverresponses.ServerResponse

import scala.concurrent.Future

/**
  * Defines the AJAX calls the client can make to the server.
  * <p>
  * Created by Matthias Braun on 1/9/2017.
  */
trait ShepAjaxApi {

  def postPersons(persons: Seq[Person]): Future[ServerResponse]
}

/**
  * Provides paths for the AJAX API.
  */
object ShepAjaxApi {
  // Used for performing Ajax calls from the client to the server
  val AjaxPath = "ajax"
}
