package shared.api

/**
  * Contains paths of the URLs we use in our application.
  * <p>
  * Created by Matthias Braun on 1/5/2017.
  */
object ShepPaths {
  val helloPage = "hello"

  val formPage = "form"

  val peopleOverviewPage = "people"

}
