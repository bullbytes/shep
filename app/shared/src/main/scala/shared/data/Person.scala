package shared.data

import java.util.UUID

/**
  * Contains data about a person.
  * <p>
  * Created by Matthias Braun on 1/7/2017.
  */
case class Person(id: UUID, name: String, age: String, occupation: String) {

  /** @return true, if any of the [[Person]]'s fields contains data */
  def nonEmpty: Boolean = name.trim.nonEmpty || age.trim.nonEmpty || occupation.trim.nonEmpty
}

