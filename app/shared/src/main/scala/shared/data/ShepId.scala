package shared.data

import java.util.UUID

/**
  * An ID for an object in this application.
  * <p>
  * Created by Matthias Braun on 1/12/2017.
  */
case class ShepId(value: String) extends AnyVal {
  override def toString: String = value
}

object ShepId {
  def from(uuid: UUID) = ShepId(uuid.toString)
}
