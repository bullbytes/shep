package shared.pages

/**
  * Identifies web pages of this application. The client uses this to know which page it got from the server.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
case class PageId(value: String) extends AnyVal {
  override def toString: String = value
}

/**
  * Contains the IDs of all our web pages.
  */
object PageIds {

  val notFound = PageId("page-not-found-id")

  val form = PageId("form-page-id")

  val hello = PageId("hello-page-id")

  val peopleOverview =PageId("people-overview-page-id")
}
