package shared.pages.styles

import scalacss.Defaults._

/**
  * Defines the look of our person form.
  * <p>
  * Created by Matthias Braun on 1/5/2017.
  */
object PeopleFormStyles extends SharedStyles {

  import dsl._

  // Style the page's body
  style(unsafeRoot("body")(
    defaultPagePadding,
    defaultBackgroundColor
  ))

  val form: StyleA = style(addClassName("pure-form"))

  private val pureButton = mixin(addClassName("pure-button"))

  private val primaryButton = mixin(addClassName("pure-button-primary"))

  val hidden: StyleA = style(addClassName("hidden"))

  val peopleOverviewLink: StyleA = style(
    display.inlineBlock,
    marginTop(30 px)
  )

  val infoBox: StyleA =
    style(
      backgroundColor.lightcyan,
      color.darkslategray,
      display.inlineBlock,
      padding(10 px, 10 px, 10 px, 10 px),
      marginTop(5 px)
    )

  val submitButton: StyleA = style(
    fontSize(150 %%),
    pureButton,
    primaryButton,
    marginTop(1 em)
  )

  val addRowButton: StyleA = style(
    marginTop(0.5 em),
    pureButton
  )

}
