package shared.pages.styles

import scalacss.Defaults._

/**
  * Styles the page giving an overview of all people saved in the database.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
object PeopleOverviewStyles extends SharedStyles {

  import dsl._
  // Style the page's body
  style(unsafeRoot("body")(
    defaultPagePadding,
    defaultBackgroundColor
  ))

  // Styles the table that shows the data of all people.
  val table: StyleA = defaultTable
}
