package shared.pages.styles

import scalacss.Defaults._
import scalacss.internal.AV

/**
  * Provides styles shared between the pages of this application.
  * <p>
  * Created by Matthias Braun on 1/21/2017.
  */
trait SharedStyles extends StyleSheet.Inline {

  import dsl._

  val defaultTable: StyleA = style(
    addClassName("pure-table"),
    margin.auto,
    fontSize(110 %%)
  )
  val defaultPagePadding: AV = padding(1 em)
  val defaultBackgroundColor: AV = backgroundColor.ivory

  val centered: StyleA = style(
    textAlign.center
  )
}

