/**
  * Configuration for this project.
  */
object BuildConfig {
  /** The applications version. We use semantic versioning */
  val appVersion = "0.0.3"

  /** The name of this application */
  val appName = "shep"
}