import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._

/**
  * Lists the dependencies for this project.
  */
object Dependencies {

  private val uPickleVersion = "0.4.4"
  private val autowireVersion = "0.2.6"
  private val sloggingVersion = "0.5.2"
  private val scalaTagsVersion = "0.6.2"
  private val scalaCssVersion = "0.5.1"

  object client {
    // JavaScript libraries the client uses
    val jsDependencies = Def.setting(Seq(
      "org.webjars" % "jquery" % "3.1.1" / "3.1.1/jquery.min.js"
    ))

    // The triple % gets the library in two versions: One for running on the JVM and one for running on a JavaScript engine like V8
    val scalaJsDependencies = Def.setting(Seq(
      // Used to produce HTML with Scala: https://github.com/lihaoyi/scalatags
      "com.lihaoyi" %%% "scalatags" % scalaTagsVersion,

      // Lets us create CSS on the client to style our websites: https://github.com/japgolly/scalacss
      "com.github.japgolly.scalacss" %%% "ext-scalatags" % scalaCssVersion,

      // Serializes data between client and server: https://github.com/lihaoyi/upickle-pprint
      "com.lihaoyi" %%% "upickle" % uPickleVersion,

      // Type-safe Ajax calls from the client to the server: https://github.com/lihaoyi/autowire
      "com.lihaoyi" %%% "autowire" % autowireVersion,

      // A type facade for jQuery so we can use the JavaScript library in a type-safe manner
      "be.doeraene" %%% "scalajs-jquery" % "0.9.1",

      // Logging: https://github.com/jokade/slogging
      "biz.enef" %%% "slogging" % sloggingVersion
    ))
  }

  val server = {
    Def.setting(Seq(
      // Our HTTP server: http://doc.akka.io/docs/akka-http/current/index.html
      "com.typesafe.akka" %% "akka-http" % "10.0.1",

      // Needed so the server can create an HTML page that it sends to the client
      "com.lihaoyi" %% "scalatags" % scalaTagsVersion,

      // Lets us create CSS on the server to style our websites: https://github.com/japgolly/scalacss
      "com.github.japgolly.scalacss" %%% "ext-scalatags" % scalaCssVersion,

      // Used for sending data in AJAX calls between client and server. Includes serialization and deserialization
      "com.lihaoyi" %% "upickle" % uPickleVersion,
      "com.lihaoyi" %% "autowire" % autowireVersion,

      //// Logging
      // Logging facade for Scala and Scala.js: https://github.com/jokade/slogging
      "biz.enef" %% "slogging-slf4j" % sloggingVersion,
      // Logging backend: http://logback.qos.ch/
      "ch.qos.logback" % "logback-classic" % "1.1.8",
      // So Akka, which provides our server, logs uing SLF4J
      "com.typesafe.akka" %% "akka-slf4j" % "2.4.16",
      // Needed for reading the logback.groovy configuration file
      "org.codehaus.groovy" % "groovy-all" % "2.4.7",

      // For connecting to our database
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0-M2",
      //"org.postgresql" % "postgresql" % "9.4.1212",
      "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"

    ))
  }
}