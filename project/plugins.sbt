// Plugins for SBT. Don't forget to leave blank lines between the plugin declarations

// Compiles Scala to JavaScript: http://www.scala-js.org
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.14")

// Creates a jar from the source code: https://github.com/sbt/sbt-native-packager
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.0-M8")

// Creates a Docker image from the jar: https://github.com/marcuslonnberg/sbt-docker
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.4.0")

// Lets us use docker compose from SBT: sbt; project appJVM; dockerComposeUp
// https://github.com/Tapad/sbt-docker-compose
addSbtPlugin("com.tapad" % "sbt-docker-compose" % "1.0.17")

// Visualizes the dependency tree of this project: https://github.com/jrudolph/sbt-dependency-graph
// Offers "dependencyTree" and "dependencyBrowseGraph" in SBT, for example
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")

// Make information about our build, such as the application name, available in the application.
// We use this, for example, to get the name of the JavaScript file that contains the transpiled Scala of our app.
// https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.6.1")

